var mongoose = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");
var Schema = mongoose.Schema;

var FarmaciaSchema = new Schema({
    nome: String,
    preferencia: String,
    stocks: [{
        apresentacao: String,
        apresentacaoId: Number,
        quantidade: Number,
        minimo: Number,
        encomendado: Boolean,
    }]
})

FarmaciaSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Farmacia', FarmaciaSchema);