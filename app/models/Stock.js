var mongoose = require('mongoose');
var mongoose_validator = require("mongoose-id-validator");
var Schema = mongoose.Schema;

var StockSchema = new Schema({
    apresentacao: String,
    apresentacaoId: String,
    quantidade: Number,
    minimo: Number,
    encomendado: Boolean,
})

StockSchema.plugin(mongoose_validator);

module.exports = mongoose.model('Stock', StockSchema);