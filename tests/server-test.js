var chai = require('chai');
var expect = chai.expect;
var app = require('../server');
var request = require('supertest');
var Farmacia = require('../app/models/Farmacia');
var bodyParser = require('body-parser');
var assert = require('assert');

describe('Server tests', function () {

    global.token;
    const nomeFarm = "MochaTest";

    it('POST /farmacia', function (done) {

        var farmPost = {
            nome: nomeFarm,
            stocks: [
                {
                    apresentacaoId: 1,
                    minimo: 1,
                    quantidade: 1,
                }
            ]
        };

        request(app).post('/api/farmacia').send(farmPost).end(function (err, res) {

            Farmacia.findOne({ nome: nomeFarm }, function (err, farmacia) {
                expect(farmacia.nome).to.equal(nomeFarm);
                Farmacia.remove(farmacia, function (err) {
                    done();
                });
            });
        });
    });

    /*it('PUT /stock/restock', function (done) {

        var nome = "Gramacho";
        var linhas = 

        request(app).post('/api/stock/restock').send({ farmacia: nome }).end(function (err, res) {

            Farmacia.findOne({nome: nome}, function(err, farmacia) {
                expect(farmacia.)
            })

        })
    });*/

    it('POST /authenticate', function (done) {

        var email = 'U2FsdGVkX19lt3/mQ9fUJLtj19/OrDKOgchjnkY/AhpC+vj+j0CpuWL4/4HfPk8U';
        var password = 'U2FsdGVkX181hPHsbxNvsm0CPcxTRHvsZAxQ6x/lC6Y=';

        request(app).post('/api/authenticate').send({ email: email, password: password }).end(function (err, res) {

            expect(res.statusCode).to.equal(200);
            expect(res.body.message).to.equal('Enjoy your token!');
            token = res.body.token;
            done();

        });
    });

    it('GET /medicamento/nome=:med/apresentacoes', function (done) {

        request(app).get('/api/medicamento/nome=Ben-u-ron/apresentacoes').end(function (err, res) {

            expect(res.body.length).to.equal(3);
            done();

        });
    });

    it('GET /receita/:receita_id', function (done) {

        var id = '5a5790309bb60b3ef8390aad';

        request(app).get('/api/receita/5a5790309bb60b3ef8390aad').set('x-access-token', token).end(function (err, res) {

            expect(res.body.id).to.equal(id);
            done();

        });
    });

    it('GET /receita/:receita_id/prescricao/:prescricao_id', function (done) {

        var r_id = '5a5790309bb60b3ef8390aad';
        var p_id = '5a5790309bb60b3ef8390aae';

        request(app).get('/api/receita/5a5790309bb60b3ef8390aad/prescricao/5a5790309bb60b3ef8390aae').set('x-access-token', token).end(function (err, res) {

            expect(res.body.id).to.equal(p_id);
            done();

        });
    });

    it('PUT /receita/:receita_id/prescricao/:prescricao_id/aviar', function (done) {

        var r_id = '5a5790309bb60b3ef8390aad';
        var p_id = '5a5790309bb60b3ef8390aae';

        request(app).put('/api/receita/5a5790309bb60b3ef8390aad/prescricao/5a5790309bb60b3ef8390aae/aviar').send({ nAviamentos: 15 }).set('x-access-token', token).end(function (err, res) {

            expect(res.body.idPrescricao).to.equal(p_id);

            Farmacia.findOne({nome: nomeFarm}, function(err, farmacia) {
                Farmacia.remove({nome: nomeFarm}, function(err) {
                    done();
                });
            });
        });
    });
});