var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var Client = require('node-rest-client').Client;
var client = new Client();
var morgan = require('morgan');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var config = require('./config');
var bcrypt = require('bcryptjs');
var eventos = require('events');
var eventEmitter = new eventos.EventEmitter();
var cors = require('cors');
var Farmacia = require('./app/models/Farmacia');
var User = require('./app/models/User');
var Stock = require('./app/models/Stock');

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8443;

mongoose.connect(config.database, { useMongoClient: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("we're connected!");
});
mongoose.Promise = global.Promise;

app.set('superSecret', config.secret);

app.use(morgan('dev'));

var router = express.Router();

router.get('/', function (req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

//cria farmácias
router.route('/farmacia')
    .post(function (req, res) {

        Farmacia.findOne({ nome: req.body.farmacia }, function (err, found) {
            if (!found) {
                var farmacia = new Farmacia();
                farmacia.nome = req.body.nome;

                for (var i = 0; i < req.body.stocks.length; i++) {
                    var stock = new Stock();

                    stock.apresentacaoId = req.body.stocks[i].apresentacaoId;
                    stock.minimo = req.body.stocks[i].minimo;
                    stock.quantidade = req.body.stocks[i].quantidade;
                    stock.encomendado = false;

                    farmacia.stocks[i] = stock;
                }

                //if (i+1 == req.body.stocks.length) {
                farmacia.save(function (err) {
                    if (err)
                        return res.status(500).json({ success: false, message: 'Não foi possivel criar a farmácia.' });

                    res.json({ message: 'Farmácia registada!' });
                });
                //}
            } else if (found) {
                return res.status(500).json({ success: false, message: "Farmácia já existente." });
                res.end();
            }
        });
    })


//aumenta a quantidade de um dado número de apresentações de fármacos na farmácia
router.route('/stock/restock')
    .put(function (req, res) {
        Farmacia.findOne({ nome: req.body.farmacia }, function (err, farmacia) {
            if (err) {
                return res.status(404).send({ success: false, message: "A farmácia " + req.farmacia + " não existe!" })
                res.end();
            }

            for (var i = 0; i < farmacia.stocks.length; i++) {
                if (farmacia.stocks[i].apresentacaoId == req.body.apresentacaoId) {
                    //Aumenta a quantidade disponivel daquela apresentacao no stock correto
                    farmacia.stocks[i].quantidade += 5;
                    farmacia.stocks[i].encomendado = false;
                }
            }
            farmacia.save(function (err) {
                if (err) {
                    return res.status(500).send(err)
                    res.end();
                }

                res.end()
            });
        })
    });

router.route('/stock/restockManual/:nome_farmacia')
    .post(function (req, res) {

        Farmacia.findOne({ nome: req.params.nome_farmacia }, function (err, farmacia) {
            if (err) {
                return res.status(404).send({ success: false, message: "A farmácia " + req.params.nome_farmacia + " não existe!" })
                res.end();
            }

            for (var i = 0; i < farmacia.stocks.length; i++) {

                if (farmacia.stocks[i].encomendado == true) {
                    //Aumenta a quantidade disponivel daquela apresentacao no stock correto
                    farmacia.stocks[i].quantidade += 5;
                    farmacia.stocks[i].encomendado = false;
                }

                farmacia.save(function (err) {
                    if (err) {
                        return res.status(500).send(err)
                        res.end();
                    }
                    return res.json({ success: true, message: "Restock realizado com sucesso" });
                    res.end()
                });
            }
        })
    })



//Autentica o farmaceutico
router.route('/authenticate')
    .post(function (req, res) {

        var email = req.body.email;
        //Expressao regular baseada no RFC 5322
        var regexpEmail = new RegExp('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/');

        var password = req.body.password;
        //Password tem de ter pelo menos 9 carateres, os quais têm de pertencer a pelo menos 3 dos 4 grupos: maiusculas, minusculas, numeros, simbolos
        var regexpPassword = new RegExp('(?=.{9,})(?=.*?[^\w\s])(?=.*?[0-9])(?=.*?[A-Z]).*?[a-z].*');

        //Verifica se o email e password fazem match com a expressão regular ou se nao foram definidos
        //        if (email == undefined || password == undefined || !regexpEmail.test(email) || !regexpPassword.test(password)) {
        //            return res.status(400).json({ success: false, message: 'Email e/ou password contêm carateres não válidos.' });
        //           res.end();
        //        }

        var args = {
            data: { email: req.body.email, password: req.body.password, farmaceutico: true },
            headers: { "Content-Type": "application/json" }
        }

        //Autentica o farmaceutico na outra aplicação
        client.post('http://gdrlapr6666.azurewebsites.net/api/authenticate', args, function (data, response) {
            return res.status(response.statusCode).json(data);
            res.end();
        });
    });

//Cria um utilizador
router.route('/user')
    .post(function (req, res) {

        var email = req.body.email;
        //Expressao regular baseada no RFC 5322
        var regexpEmail = new RegExp('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/');

        var password = req.body.password;
        //Password tem de ter pelo menos 9 carateres, os quais têm de pertencer a pelo menos 3 dos 4 grupos: maiusculas, minusculas, numeros, simbolos
        var regexpPassword = new RegExp('(?=.{9,})(?=.*?[^\w\s])(?=.*?[0-9])(?=.*?[A-Z]).*?[a-z].*');

        var name = req.body.name;
        var farmacia = req.body.farmacia;
        //Nome do utilizador e farmácia não podem conter algarismos
        var regexpName = new RegExp('^[\p{L}\s\'.-]+$');

        //Verifica se o email faz match com a expressão regular
        //        if (email == undefined || password == undefined || name == undefined || req.body.farmacia == undefined || !regexpEmail.test(email) || !regexpPassword.test(password) || !regexpName.test(name) || !regexpName.test(farmacia)) {
        //            return res.status(400).json({ success: false, message: 'Reveja os campos introduzidos.' });
        //            res.end();
        //        }

        var args = {
            data: { email: req.body.email, name: req.body.name, farmacia: req.body.farmacia, password: req.body.password, farmaceutico: true },
            headers: { "Content-Type": "application/json" }
        }

        Farmacia.find({ name: req.body.farmacia }, function (err, farmacia) {
            if (err) {
                return res.status(404).json({ success: false, message: "Farmácia não existente." });
                res.end();
            }
        });

        //Cria o utilizador na GdR
        client.post('http://gdrlapr6666.azurewebsites.net/api/User', args, function (data, response) {
            return res.status(response.statusCode).json(data);
            res.end();
        });
    });

router.route('/medicamento/nome=:med/apresentacoes')
    .get(function (req, res) {

        var args = {
            path: { "med": req.params.med }
        };

        client.get('http://gdrlapr6666.azurewebsites.net/api/Medicamento/nome=${med}/apresentacoes', args, function (data, response) {
            return res.status(response.statusCode).json(data);
            res.end();
        });
    });

//rota que interceta todos os pedidos abaixo dela para verificar o token apresentado
router.use(function (req, res, next) {

    //procura o token nos headers do pedido HTTP, nos parametros do URL ou nos parametros do POST
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    //se é apresentado um token
    if (token) {

        //desencripta o token
        jwt.verify(token, app.get('superSecret'), function (err, decoded) {
            if (err) {
                return res.status(500).json({ success: false, message: 'Falha na autenticação do token.' });
            } else {
                //se estiver bem construido, guarda-o para uso em outros pedidos
                req.decoded = decoded;
                next();
            }
        });

    } else {

        //se nao for apresentado token, retorna erro
        return res.status(403).send({
            success: false,
            message: 'Não foi apresentado um token.'
        });
    }
});

//Apresenta a receita com o ID escolhido
router.route('/receita/:receita_id')
    .get(function (req, res) {

        //Encontra o token apresentado
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        args = {
            path: { "r_id": req.params.receita_id },
            headers: { "x-access-token": token }
        }

        //GET da receita especificada, usando o token obtido
        client.get('http://gdrlapr6666.azurewebsites.net/api/receita/${r_id}', args, function (data, response) {
            return res.status(response.statusCode).json(data);
            res.end();
        });
    });

router.route('/receita/:receita_id/prescricao/:prescricao_id')
    .get(function (req, res) {

        //Encontra o token apresentado
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        args = {
            path: { "r_id": req.params.receita_id, "p_id": req.params.prescricao_id },
            headers: { "x-access-token": token }
        };

        //GET da prescricao especificada, usando o token obtido
        client.get('http://gdrlapr6666.azurewebsites.net/api/receita/${r_id}/prescricao/${p_id}', args, function (data, response) {
            return res.status(response.statusCode).json(data);
            res.end();
        });
    });

//Avia uma prescrição de uma determinada receita, na farmacia correta
router.route('/receita/:receita_id/prescricao/:prescricao_id/aviar')
    .put(function (req, res) {

        //Encontra o token apresentado
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        //Desencripta o token
        jwt.verify(token, app.get('superSecret'), function (err, decoded) {

            var args = {
                path: { "r_id": req.params.receita_id, "p_id": req.params.prescricao_id },
                headers: { "x-access-token": token }
            }

            client.get('http://gdrlapr6666.azurewebsites.net/api/receita/${r_id}/prescricao/${p_id}', args, function (data, response) {

                if (response.statusCode == 500 || response.statusCode == 404) {
                    return res.status(response.statusCode).send({ success: false, message: "Receita ou prescrição não encontrada." });
                }

                var apresentacaoId = data.apresentacaoId;

                //Procura na base de dados a farmacia a que o farmaceutico pertence
                Farmacia.findOne({ nome: decoded.farmacia }, function (err, farmacia) {
                    if (err) {
                        return res.status(500).send({ success: false, message: "Erro a procurar farmácia." })
                    }

                    for (var i = 0; i < farmacia.stocks.length; i++) {

                        //Encontra na farmacia, o stock do qual a prescricao vai consumir (identificado pela apresentacaoId)
                        if (farmacia.stocks[i].apresentacaoId == apresentacaoId && (farmacia.stocks[i].quantidade - req.body.nAviamentos) >= 0) {

                            //Diminui a quantidade disponivel daquela apresentacao no stock correto
                            farmacia.stocks[i].quantidade -= req.body.nAviamentos;

                            farmacia.save(function (err) {
                                if (err) {
                                    return res.status(500).send({ success: false, message: "Não foi possível guardar a nova quantidade deste stock na farmácia." })
                                }
                            });

                            //Verifica se a quantidade de uma determinada apresentacao é inferior ao minimo
                            if (farmacia.stocks[i].quantidade <= farmacia.stocks[i].minimo && farmacia.stocks[i].encomendado == false) {

                                //Encomenda, para aquela farmacia e para aquele stock mais apresentacoes
                                encomendar(farmacia, i);
                            }

                            args = {
                                data: { "nAviamentos": req.body.nAviamentos },
                                path: { "r_id": req.params.receita_id, "p_id": req.params.prescricao_id },
                                headers: { "x-access-token": token, "Content-Type": "application/json" }
                            }

                            client.put('http://gdrlapr6666.azurewebsites.net/api/receita/${r_id}/prescricao/${p_id}/aviar', args, function (data, response) {

                                if (response.statusCode == 404 || response.statusCode == 500 || response.statusCode == 403) {
                                    return res.status(response.statusCode).send(data);
                                }

                                return res.json(data);
                                res.end();
                            });

                            break;
                        }
                    }
                })
            })
        });
    });

//Encomenda apresentacoes para um stock da farmacia
function encomendar(farmacia, stock) {

    farmacia.stocks[stock].encomendado = true;

    args = {
        data: { "farmacia": farmacia.nome, "preferencia": farmacia.preferencia, "apresentacaoId": farmacia.stocks[stock].apresentacaoId}
    }

    client.post('http://fornecedorlapr6666.azurewebsites.net/api/Encomendas', args, function (data, response) {

        /* if (response.statusCode == 404 || response.statusCode == 500 || response.statusCode == 403) {
             return res.status(response.statusCode).send(data);
         }
 
         return res.json({ success: true, message: "Encomenda enviada com sucesso." })
         res.end();*/
    });

    return;
}

app.use('/api', router);

app.listen(port);
console.log('Magic happens on port ' + port);

module.exports = app;